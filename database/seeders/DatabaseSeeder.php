<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->createRoles();

        $admin = \App\Models\User::factory(1)->create([
            'email' => 'admin@abcare.vn',
        ])->first();

        $admin->assignRole('admin');
    }

    public function createRoles() {
        Role::create(['name' => 'customer','description' => 'Khách hàng']);
        Role::create(['name' => 'seller', 'description' => 'Đại lý']);
        Role::create(['name' => 'partner', 'description' => 'Cộng tác viên']);
        Role::create(['name' => 'sales', 'description' => 'Kinh doanh']);
        Role::create(['name' => 'marketing', 'description' => 'Quảng cáo']);
        Role::create(['name' => 'support', 'description' => 'Hỗ trợ']);
        Role::create(['name' => 'accountant', 'description' => 'Kế toán']);
        Role::create(['name' => 'admin', 'description' => 'Quản trị']);
    }
}
