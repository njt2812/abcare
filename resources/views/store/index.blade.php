<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>ABCare Store</title>
  <script src="x-frame-bypass.js" type="module"></script>
  <style>
        html, body {
            margin: 0;
            padding: 0;
            height: 100%;
            overflow: hidden;
        }
        iframe {
            display: block;
            width: calc(100% - 40px);
            height: calc(100% - 40px);
            margin: 20px;
            border: 0;
        }

    </style>
</head>
<body>
  <iframe is="x-frame-bypass" src="https://kalles-niche.myshopify.com/?_ab=0&_fd=0&_sc=1" frameborder="0"></iframe>
</body>
</html>