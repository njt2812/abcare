<div>
  <div class="w-40 h-40 relative image-fit cursor-pointer zoom-in mx-auto">

    @if ($newAvatar)
        <img class="rounded-md" alt="Avatar" src="{{ $newAvatar->temporaryUrl() }}">
    @else
        <img class="rounded-md" alt="Avatar" src="{{ asset('dist/images/' . $fakers[0]['photos'][0]) }}">
    @endif

  </div>
  <div class="w-40 mx-auto cursor-pointer relative mt-5">
    <button type="button" class="button w-full bg-theme-1 text-white">Ảnh đại diện</button>
    <input type="file" wire:model='newAvatar' class="w-full h-full top-0 left-0 absolute opacity-0">
  </div>
</div>
