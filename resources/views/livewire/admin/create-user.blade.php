<div>
  <div class="grid grid-cols-12 gap-6">
    <div class="col-span-12 ">
      <!-- BEGIN: Display Information -->
      <div class="intro-y box lg:mt-5">
        <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">
          <h2 class="font-medium text-base mr-auto">Thông tin hệ thống</h2>
        </div>
        <div class="p-5">
          <div class="grid grid-cols-12 gap-5">
            <div class="col-span-12 xl:col-span-4">
              <div id="avatar-div" data-single="true" class="border border-gray-200 dark:border-dark-5 rounded-md p-5">
                <div class="w-40 h-40 relative image-fit cursor-pointer zoom-in mx-auto">

                  @if ($newAvatar)
                    <img class="rounded-md" alt="Avatar" src="{{ $newAvatar->temporaryUrl() }}">
                  @else
                    <img class="rounded-md" alt="Avatar" src="{{ asset('dist/images/' . $fakers[0]['photos'][0]) }}">
                  @endif

                </div>
                <div class="w-40 mx-auto cursor-pointer relative mt-5">
                  <button type="button" class="button w-full bg-theme-1 text-white">Ảnh đại diện</button>
                  <input type="file" wire:model='newAvatar' class="w-full h-full top-0 left-0 absolute opacity-0">
                </div>
              </div>
            </div>
            <div class="col-span-12 xl:col-span-8">
              <div>
                <label>Địa chỉ email</label>
                <input name="email" type="text" class="input w-full border  mt-2" value="{{ old('email') }}">
                @error('email')
                <div class="pristine-error text-theme-6 mt-2">Địa chỉ email không hợp lệ</div>
                @enderror
              </div>
              <div class="mt-3">
                <label>Mật khẩu</label>
                <input name="password" type="password" class="input w-full border  mt-2" value="{{ old('password') }}">
                @error('email')
                <div class="pristine-error text-theme-6 mt-2">Mật khẩu không hợp lệ</div>
                @enderror
              </div>
              <div class="mt-3">
                <label>Nhắc lại mật khẩu</label>
                <input name="password-confirm" type="password" class="input w-full border  mt-2"
                  value="{{ old('password') }}">
                @error('email')
                <div class="pristine-error text-theme-6 mt-2">Nhắc lại mật khẩu không hợp lệ</div>
                @enderror
              </div>

              <div class="mt-3">
                <label>Chức danh</label>
                <div class="mt-2">
                  <select name="role" data-search="true" class="input border w-full">
                    @foreach ($roles as $role)
                      <option value="{{ $role->id }}">{{ ucfirst($role->description) }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- END: Display Information -->
      <!-- BEGIN: Personal Information -->
      <div class="intro-y box lg:mt-5">
        <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">
          <h2 class="font-medium text-base mr-auto">Thông tin cá nhân</h2>
        </div>
        <div class="p-5">
          <div class="grid grid-cols-12 gap-5">
            <div class="col-span-12 xl:col-span-6">
              <div>
                <label>Họ và Tên</label>
                <input name="name" type="text" class="input w-full border mt-2" value="{{ old('name') }}">
              </div>
              <div class="mt-3">
                <label>Số CMND</label>
                <input name="passport_id" type="text" class="input w-full border mt-2" value="{{ old('passport_id') }}">
              </div>
              <div class="mt-3">

                <label>Điện thoại</label>
                <input name="phone" type="text" class="input w-full border mt-2" value="{{ old('phone') }}">

              </div>
            </div>
            <div class="col-span-12 xl:col-span-6">
              <div>
                <label>Facebook</label>
                <input name="facebook_url" type="text" class="input w-full border mt-2"
                  value="{{ old('facebook_url') }}">
              </div>
              <div class="mt-3">
                <label>Tên ngân hàng</label>
                <input name="bank_name" type="text" class="input w-full border mt-2" value="{{ old('bank_name') }}">
              </div>
              <div class="mt-3">
                <label>Số tài khoản ngân hàng</label>
                <input name="bank_number" type="text" class="input w-full border mt-2" value="{{ old('bank_number') }}">
              </div>
            </div>
          </div>

        </div>
      </div>
      <!-- END: Personal Information -->
      <!-- BEGIN: Address Information -->
      <div class="intro-y box lg:mt-5">
        <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">
          <h2 class="font-medium text-base mr-auto">Thông tin địa chỉ</h2>
        </div>
        <div class="p-5">
          <div class="grid grid-cols-12 gap-5">
            <div class="col-span-12 xl:col-span-6">
              <div>
                <label>Tỉnh/Thành phố</label>
                <div class="mt-2">
                  <select wire:model="selectedProvince" data-search="true" class="input border w-full">
                    @foreach ($provinces as $key => $province)
                      <option wire:key="{{ $province->provinceid }}" value="{{ $province->provinceid }}">
                        {{ $province->name }}</option>
                    @endforeach
                    {{-- <option wire:key="46TTT" value="46TTT">Tỉnh Thừa Thiên Huế
                    </option>
                    <option wire:key="48TTT" value="48TTT">Thành phố Đà Nẵng</option> --}}
                  </select>
                </div>
              </div>
              <div class="mt-3">
                <label>Quận/Huyện</label>
                <div class="mt-2">
                  <select wire:model="selectedDistrict" data-search="true" class="input border w-full">
                    @foreach ($districts as $district)
                      <option wire:key="{{ $district->districtid }}" value="{{ $district->districtid }}">
                        {{ $district->name }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
            <div class="col-span-12 xl:col-span-6">
              <div>
                <label>Xã/Phường/Thị trấn</label>
                <div class="mt-2">
                  <select wire:model="selectedWard" data-search="true" class="input border w-full">
                    @foreach ($wards as $ward)
                      <option wire:key="{{ $ward->wardid }}" value="{{ $ward->wardid }}">{{ $ward->name }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="mt-3">
                <label>Tổ/Xóm/Khu phố</label>
                <div class="mt-2">
                  <select data-search="true" class="input border w-full">
                    @foreach ($villages as $village)
                      <option wire:key="{{ $village->villageid }}" value="{{ $village->villageid }}">
                        {{ $village->name }}
                      </option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="mt-3">
                <label>Số nhà - Đường</label>
                <input type="text" class="input w-full border  mt-2" placeholder="">
              </div>
            </div>
          </div>
        </div>
        <!-- END: Address Information -->
        <div class="flex justify-end mt-4">
          {{-- <a href="" class="text-theme-6 flex items-center">
            <i data-feather="trash-2" class="w-4 h-4 mr-1"></i> Delete Account
          </a> --}}
          <button type="submit" class="button w-20 bg-theme-1 text-white ml-auto">Save</button>
        </div>
      </div>
    </div>
  </div>
