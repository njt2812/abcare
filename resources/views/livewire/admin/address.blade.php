<div>
  <!-- BEGIN: Address Information -->
  <div class="intro-y box lg:mt-5">
    <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">
      <h2 class="font-medium text-base mr-auto">Thông tin địa chỉ</h2>
    </div>
    <div class="p-5">
      <div class="grid grid-cols-12 gap-5">
        <div class="col-span-12 xl:col-span-6">
          <div>
            <label>Tỉnh/Thành phố</label>
            <div class="mt-2">
              <select wire:model="selectedProvince" data-search="true" class="input border w-full">
                @foreach ($provinces as $key => $province)
                  <option wire:key="{{ $province->provinceid }}" value="{{ $province->provinceid }}">
                    {{ $province->name }}</option>
                @endforeach
                {{-- <option wire:key="46TTT" value="46TTT">Tỉnh Thừa Thiên Huế</option>
                <option wire:key="48TTT" value="48TTT">Thành phố Đà Nẵng</option> --}}
              </select>
            </div>
          </div>
          <div class="mt-3">
            <label>Quận/Huyện</label>
            <div class="mt-2">
              <select wire:model="selectedDistrict" data-search="true" class="input border w-full">
                @foreach ($districts as $district)
                  <option wire:key="{{ $district->districtid }}" value="{{ $district->districtid }}">
                    {{ $district->name }}</option>
                @endforeach
              </select>
            </div>
          </div>
        </div>
        <div class="col-span-12 xl:col-span-6">
          <div>
            <label>Xã/Phường/Thị trấn</label>
            <div class="mt-2">
              <select wire:model="selectedWard" data-search="true" class="input border w-full">
                @foreach ($wards as $ward)
                  <option wire:key="{{ $ward->wardid }}" value="{{ $ward->wardid }}">{{ $ward->name }}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="mt-3">
            <label>Tổ/Xóm/Khu phố</label>
            <div class="mt-2">
              <select data-search="true" class="input border w-full">
                @foreach ($villages as $village)
                  <option wire:key="{{ $village->villageid }}" value="{{ $village->villageid }}">{{ $village->name }}
                  </option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="mt-3">
            <label>Số nhà - Đường</label>
            <input type="text" class="input w-full border  mt-2" placeholder="">
          </div>
        </div>
      </div>
    </div>
    <!-- END: Address Information -->
  </div>
