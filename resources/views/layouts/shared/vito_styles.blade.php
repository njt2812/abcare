<!-- Favicon -->
  <link rel="shortcut icon" href="{{asset('admin/images/favicon.ico')}}" />
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="{{asset('admin/css/bootstrap.min.css')}}">
  <!-- Typography CSS -->
  <link rel="stylesheet" href="{{asset('admin/css/typography.css')}}">
  <!-- Style CSS -->
  <link rel="stylesheet" href="{{asset('admin/css/style.css')}}">
  <!-- Responsive CSS -->
  <link rel="stylesheet" href="{{asset('admin/css/responsive.css')}}">
  <!-- Full calendar -->
  <link href='{{asset('admin/fullcalendar/core/main.css')}}' rel='stylesheet' />
  <link href='{{asset('admin/fullcalendar/daygrid/main.css')}}' rel='stylesheet' />
  <link href='{{asset('admin/fullcalendar/timegrid/main.css')}}' rel='stylesheet' />
  <link href='{{asset('admin/fullcalendar/list/main.css')}}' rel='stylesheet' />

  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">