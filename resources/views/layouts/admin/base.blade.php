<!DOCTYPE html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="{{ $dark_mode ? 'dark' : '' }}">
<!-- BEGIN: Head -->

<head>
  <meta charset="utf-8">
  <link href="{{ asset('dist/images/Logo-ABCare.png') }}" rel="shortcut icon">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  @yield('head')

  <!-- BEGIN: CSS Assets-->
  <link rel="stylesheet" href="{{ mix('dist/css/app.css') }}" />
  <!-- END: CSS Assets-->
  @livewireStyles
  <!-- Scripts -->
  <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.6.0/dist/alpine.js"></script>
</head>
<!-- END: Head -->


<body class="@yield('body_class','app')">

  @yield('content')
  {{-- @include('../layout/components/dark-mode-switcher')
  --}}

  <!-- BEGIN: JS Assets-->
  <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js">
  </script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBG7gNHAhDzgYmq4-EHvM4bqW1DNj2UCuk&libraries=places">
  </script>

  <script src="{{ mix('dist/js/app.js') }}"></script>
  
  <!-- END: JS Assets-->
  @livewireScripts
  <script src="/cyclic.js"></script>
  @yield('script')
</body>

</html>
