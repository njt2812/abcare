@extends('layouts.admin.base')

@section('head')
  <title>Login - ABCare</title>
@endsection

@section('body_class')
  login
@endsection

@section('content')
  <div class="container sm:px-10">
    <div class="block xl:grid grid-cols-2 gap-4">
      <!-- BEGIN: Login Info -->
      <div class="hidden xl:flex flex-col min-h-screen">
        <a href="" class="-intro-x flex items-center pt-5">
          <img alt="Abcare logo" class="w-6" src="{{ asset('dist/images/Logo-ABCare.png') }}">
          <span class="text-white text-lg ml-3">
            ABC<span class="font-medium">are</span>
          </span>
        </a>
        <div class="my-auto">
          <img alt="Abcare logo" class="-intro-x w-1/2 -mt-16" src="{{ asset('dist/images/illustration.svg') }}">
          <div class="-intro-x text-white font-medium text-4xl leading-tight mt-10">Hệ thống quản lý doanh nghiệp ABCare.
          </div>
          <div class="-intro-x mt-5 text-lg text-white dark:text-gray-500">Quản lý tất cả nghiệp vụ chỉ một nơi duy nhất.
          </div>
        </div>
      </div>
      <!-- END: Login Info -->
      <!-- BEGIN: Login Form -->
      <form class="h-screen xl:h-auto flex py-5 xl:py-0 my-10 xl:my-0" method="post">
        @csrf

        <div
          class="my-auto mx-auto xl:ml-20 bg-white xl:bg-transparent px-5 sm:px-8 py-8 xl:p-0 rounded-md shadow-md xl:shadow-none w-full sm:w-3/4 lg:w-2/4 xl:w-auto">
          <h2 class="intro-x font-bold text-2xl xl:text-3xl text-center xl:text-left">Đăng Nhập</h2>
          <div class="intro-x mt-2 text-gray-500 xl:hidden text-center">Quản lý tất cả nghiệp vụ chỉ một nơi duy nhất.
          </div>
          <div class="intro-x mt-8">
            <form action="" id="login-form">
              @csrf
              @method('POST')

              <input type="text" name="email" id="input-email"
                class="intro-x login__input input input--lg border border-gray-300 block" placeholder="Email"
                value="admin@abcare.vn">

              @error('email')
              <div id="error-email" class="login__input-error w-5/6 text-theme-6 mt-2">{{ $message }}</div>
              @enderror


              <input type="password" name="password" id="input-password"
                class="intro-x login__input input input--lg border border-gray-300 block mt-4" placeholder="Password"
                value="password">

              @error('password')
              <div id="error-password" class="login__input-error w-5/6 text-theme-6 mt-2"></div>
              @enderror
            </form>
          </div>
          <div class="intro-x flex text-gray-700 dark:text-gray-600 text-xs sm:text-sm mt-4">
            <div class="flex items-center mr-auto">
              <input name="remember" type="checkbox" class="input border mr-2" id="input-remember-me">
              <label class="cursor-pointer select-none" for="input-remember-me">Ghi nhớ đăng nhập</label>
            </div>
            <a href="#">Quên mật khẩu?</a>
          </div>
          <div class="intro-x mt-5 xl:mt-8 text-center xl:text-right">
            <button id="btn-login" type="submit"
              class="button button--lg w-full xl:w-32 text-white bg-theme-1  align-top">Đăng nhập</button>
          </div>
        </div>

      </form>
      <!-- END: Login Form -->
    </div>
  </div>
@endsection

@section('script')
  <script>
    // cash(function() {
    //   async function login() {
    //     // Reset state
    //     cash('#login-form').find('.input').removeClass('border-theme-6')
    //     cash('#login-form').find('.login__input-error').html('')

    //     // Post form
    //     let email = cash('#input-email').val()
    //     let password = cash('#input-password').val()
    //     let rememberMe = cash('#input-remember-me').val()

    //     // Loading state
    //     cash('#btn-login').html('<i data-loading-icon="tail-spin" data-color="white" class="w-5 h-5 mx-auto"></i>')
    //       .svgLoader()
    //     await helper.delay(1500)

    //     axios.post(`login`, {
    //       email: email,
    //       password: password,
    //       remember_me: rememberMe
    //     }).then(res => {
    //       location.href = '/'
    //     }).catch(err => {
    //       cash('#btn-login').html('Login')
    //       if (err.response.data.message != 'Wrong email or password.') {
    //         for (const [key, val] of Object.entries(err.response.data.errors)) {
    //           cash(`#input-${key}`).addClass('border-theme-6')
    //           cash(`#error-${key}`).html(val)
    //         }
    //       } else {
    //         cash(`#input-password`).addClass('border-theme-6')
    //         cash(`#error-password`).html(err.response.data.message)
    //       }
    //     })
    //   }

    //   cash('#login-form').on('keyup', function(e) {
    //     if (e.keyCode === 13) {
    //       login()
    //     }
    //   })

    //   cash('#btn-login').on('click', function() {
    //     login()
    //   })
    // })

  </script>
@endsection
