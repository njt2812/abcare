@extends('layouts.admin.side-menu')

@section('subhead')
  <title>Thêm thành viên mới - ABCare</title>

@endsection

@section('subcontent')
  @livewire('admin.create-user')
@endsection
