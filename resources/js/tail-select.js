import tail from 'tail.select'

(function(cash) { 
    "use strict";
        
    // Tail Select
    cash('.tail-select').each(function() {
        let configs = {}

        if (cash(this).data('placeholder')) {
            configs.placeholder = cash(this).data('placeholder')
        }

        if (cash(this).attr('class') !== undefined) {
            configs.classNames = cash(this).attr('class')
        }

        if (cash(this).data('search')) {
            configs.search = true
        }

        if (cash(this).attr('multiple') !== undefined) {
            configs.descriptions = true
            configs.hideSelected = true
            configs.hideDisabled = true
            configs.multiLimit = 15
            configs.multiShowCount = false
            configs.multiContainer = true
        }

        tail(this, configs)
    })
})(cash)