<?php

namespace App\Http\Livewire\Admin;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Livewire\Component;

class Signin extends Component
{
    use AuthenticatesUsers;
    // public $redirectTo = '/admin';

   

    public $email;
    public $password;
    public $remember;

    public function render()
    {
        return view('livewire.admin.signin')->extends('layouts.app');
    }

    public function signin() {

        $this->login(request());
    }
}
