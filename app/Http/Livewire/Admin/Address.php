<?php

namespace App\Http\Livewire\Admin;

use App\Models\District;
use App\Models\Province;
use App\Models\Village;
use App\Models\Ward;
use Livewire\Component;

class Address extends Component
{
    
    public $selectedProvince = '79TTT';
    public $selectedDistrict = '';
    public $selectedWard= '';
    public function mount()
    {
        $this->selectedDistrict = District::where('provinceid', $this->selectedProvince)->first()->districtid;
        $this->selectedWard = Ward::where('districtid', $this->selectedDistrict)->first()->wardid;
    }

    public function render()
    {
        return view('livewire.admin.address',[
            'provinces' => Province::all(),
            'districts' => District::where('provinceid', $this->selectedProvince)->get(),
            'wards' => Ward::where('districtid', $this->selectedDistrict)->get(),
            'villages' => Village::where('wardid', $this->selectedWard)->get(),
            ]);
    }
}
