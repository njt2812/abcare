<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use Livewire\WithFileUploads;

class UploadAvatar extends Component
{
    use WithFileUploads;
    public $newAvatar;
    
    public function render()
    {
        return view('livewire.admin.upload-avatar');
    }
}
