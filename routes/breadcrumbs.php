<?php

use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;

Breadcrumbs::for('admin.', function($trail){
  $trail->push('Trang chủ','/admin-panel');
});

Breadcrumbs::for('admin.dashboard', function ($trail) {
  $trail->parent('admin.');
  $trail->push('Thống kê', route('admin.dashboard'));
});


Breadcrumbs::for('admin.users.create', function ($trail) {
  $trail->parent('admin.');
  $trail->push('Thêm thành viên', route('admin.users.create'));
});