<?php

use App\Http\Controllers\Admin\AuthController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Livewire\Admin\Signin;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('admin-panel')->group(function () {
  Route::name('admin.')->group(function () {

    Route::get('/', function(){
      return redirect()->route('admin.dashboard');
    });
    
    Route::middleware(['guest'])->group(function(){
      // Login action
      Route::get('/login', [AuthController::class, 'index'])->name('show.login');
      Route::post('/login', [AuthController::class, 'login']);

    });
    

    Route::middleware(['auth'])->group(function () {

      Route::get('/dashboard', function () {
        return view('admin.dashboard');
      })->name('dashboard');

      // Begin admin user routes
      Route::prefix('/users')->group(function(){
        Route::name('users.')->group(function(){
          Route::post('/logout', [AuthController::class, 'logout'])->name('logout');

          Route::get('/create', [UserController::class, 'create'])->name('create');
          Route::post('/create', [UserController::class, 'store'])->name('create');

        });
      });
      // End admin user routes

      
    });
  });
});